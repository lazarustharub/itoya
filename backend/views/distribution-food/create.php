<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionFood */

$this->title = 'Create Distribution Food';
$this->params['breadcrumbs'][] = ['label' => 'Distribution Foods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-food-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
