<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionFood */

$this->title = 'Update Distribution Food: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Distribution Foods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribution-food-update">

    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>

</div>
