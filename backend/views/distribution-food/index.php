<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DistributionFoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Distribution Foods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-food-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Distribution Food', ['create?id='.$category_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            // 'picture',
            [
              'label' => 'Gambar',
              // 'attribute' => 'picture',
              'format' => ['image',['width'=>'150','height'=>'150']],
              'headerOptions' => ['style' => 'width:10%'],
              'value' => function($data) {
                return Yii::$app->urlManager->createUrl(['/images/food/'.$data->picture]);
              },
            ],
            // [
            //   'label' => 'Kota',
            //   'attribute' => 'city_id',
            //   'value' => 'city.name',
            //   'filter'=>ArrayHelper::map(\common\models\DistributionCity::find()->asArray()->all(), 'id', 'name'),
            // ],

            [
                      'class' => 'yii\grid\ActionColumn',
                      'header' => 'Actions',
                      'headerOptions' => ['style' => 'color:#337ab7'],
                      'template' => '{update} {delete}',
                      'buttons' => [

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'Hapus'),
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                                        'data-method' => 'post', 'data-pjax' => '0',
                            ]);
                        }

                      ],
                      'urlCreator' => function ($action, $model, $key, $index) {

                        if ($action === 'update') {
                            $url = Url::to(['distribution-food/update','id'=>$model['id'], 'category_id'=>$_GET['id']]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = Url::to(['distribution-food/delete','id'=>$model['id'], 'category_id'=>$_GET['id']]);
                            return $url;
                        }

                      }
                      ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
