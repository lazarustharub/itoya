<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionFood */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribution-food-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <div class="box box-primary">
      <div class="box-header with-border">
        <label>Formulir Makanan Baru</label>
      </div>
      <div class="box-body">
        <div class="form-group">
          <div class="col-md-2">
            <label>Nama Makanan</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label>Picture</label>
          </div>
          <div class="col-md-10">
            <?php echo FileInput::widget([
              'model' => $model,
              'attribute' => 'gambar',
              'pluginOptions' => [
                'initialPreview'=>[
                  Html::img(Yii::$app->urlManager->createUrl(['/images/food/'.$model->picture]),['style' => 'width:200px;'])
                ],
                'overwriteInitial'=>true
              ]
            ]); ?>
          </div>
        </div>


        <div class="form-group">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
      </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
