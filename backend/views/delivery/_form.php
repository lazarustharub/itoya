<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-primary">

      <div class="box-body">
        <div class="form-group">
          <div class="col-md-2">
            <label>Nama Layanan</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label>Link</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label>Logo</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'gambar')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showUpload' => false,
                //    'showPreview' => false,
                    // 'browseLabel' => '',
                    // 'removeLabel' => '',
                    'mainClass' => 'input-group-lg'
                ]
            ])->label(false) ?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
