<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delivery Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <p>
          <?= Html::a('Create Services', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>
      <div class="box-body">

        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'name',
            [
              'label' => 'Nama Servis',
              'attribute' => 'name',
              'headerOptions' => ['style' => 'width:50%'],
            ],
            [
                   'label'=>'Link',
                   'attribute' => 'link',
                   'format' => 'url',
                   'value'=>function ($data) {
                        return $data->link;
                    },
            ],
            // 'picture',
            [
              'label' => 'Logo',
              'format' => ['image',['width'=>'100','height'=>'100']],
              'headerOptions' => ['style' => 'width:10%'],
              'value' => function($data) {
                return Yii::$app->urlManager->createUrl(['/images/delivery/'.$data->picture]);
              },
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
          ],
        ]); ?>

      </div>
    </div>

    <?php Pjax::end(); ?>
</div>
