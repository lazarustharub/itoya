<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">

      <div class="box-body">
        <div class="form-group">
          <div class="col-md-2">
            <label>Nama Layanan</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label>Link</label>
          </div>
          <div class="col-md-10">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label>Logo</label>
          </div>
          <div class="col-md-10">
          <?php echo FileInput::widget([
                  'model' => $model,
                  'attribute' => 'gambar',
                  'pluginOptions' => [
                      'showUpload' => false,
                      'initialPreview'=>[
                          Html::img(Yii::$app->urlManager->createUrl(['/images/delivery/'.$model->picture]),['style' => 'width:200px;'])
                      ],
                      'overwriteInitial'=>true
                  ]
              ]); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
