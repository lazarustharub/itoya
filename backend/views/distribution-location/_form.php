<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionLocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribution-location-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <label>Formulir Lokasi Baru</label>
      </div>
      <div class="box-body">
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Nama Lokasi</label>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Telephone</label>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'phone')->textInput(['type' => 'number','min'=>0])->label(false) ?>
          </div>
        </div>
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Kota</label>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'city_id')->dropDownList(
            ArrayHelper::map(\common\models\DistributionCity::find()->asArray()->all(), 'id', 'name'),['prompt'=>"Pilih Kota ..."]
            )->label(false) ?>
          </div>
        </div>
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Lokasi</label>
          </div>
          <div class="col-md-10">
            <?=
              $form->field($model, 'location')->widget(\kalyabin\maplocation\SelectMapLocationWidget::className(), [
                  'attributeLatitude' => 'location_x',
                  'attributeLongitude' => 'location_y',
                  'googleMapApiKey' => 'AIzaSyDEJifTz-2J9QyeCN9F45uNcSozkeLqSaI',
                  'textOptions' => ['required'=>true,'style'=>'width:100%; height:40px;'],
                  'wrapperOptions' => ['style'=>'width: 100%; height: 250px;']
              ])->label(false);
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-body">
        <div class="col-md-6">
          <?= $form->field($model, 'open_hour')->widget(TimePicker::classname(), []) ?>
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'close_hour')->widget(TimePicker::classname(), []) ?>
        </div>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-body">
        <div class="col-md-12">
          <label>Foto</label>
        </div>
        <div class="col-md-12">
          <?= $form->field($model, 'gambar')->widget(FileInput::classname(), [
              'options' => ['accept' => 'image/*','required'=>true],
              'pluginOptions' => [
                  'showUpload' => false,
              //    'showPreview' => false,
                  // 'browseLabel' => '',
                  // 'removeLabel' => '',
                  'mainClass' => 'input-group-lg'
              ]
          ])->label(false) ?>
          <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          </div>
      </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
