<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionLocation */

$this->title = 'Create Distribution Location';
$this->params['breadcrumbs'][] = ['label' => 'Distribution Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-location-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
