<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionLocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribution-location-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'open_hour') ?>

    <?= $form->field($model, 'close_hour') ?>

    <?= $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'picture') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'location_x') ?>

    <?php // echo $form->field($model, 'location_y') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
