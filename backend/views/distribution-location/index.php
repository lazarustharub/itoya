<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DistributionLocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Distribution Locations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-location-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Distribution Location', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' =>[
        'style'=>'width:100%'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            // 'open_hour',
            // 'close_hour',
            'phone',
            //'picture',
            // 'location',
            [
              'header' => 'Lokasi',
              'headerOptions' => ['style'=>'width:30%'],
              'attribute' => 'location',
            ],
            //'location_x',
            //'location_y',
            [
              'label' => 'Kota',
              'attribute' => 'city_id',
              'value' => 'city.name',
              'filter'=>ArrayHelper::map(\common\models\DistributionCity::find()->asArray()->all(), 'id', 'name'),
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
