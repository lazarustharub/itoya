<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionLocation */

$this->title = 'Update Distribution Location: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Distribution Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribution-location-update">

    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>

</div>
