<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromotionVideo */

$this->title = 'Create Promotion Video';
$this->params['breadcrumbs'][] = ['label' => 'Promotion Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-video-create">

  <div class="box box-primary">
    <div class="box-body">
      <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
  </div>

</div>
