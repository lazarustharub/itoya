<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\PromotionVideo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promotion-video-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <h4>Preview Update</h4>

    <div class="col-md-12" align="center">
      <video max-width="600px" height="auto" controls style="border:2px solid #3c8dbc;" >
        <source src="<?=Yii::$app->urlManager->createUrl(['/videos/'.$oldVideo])?>" type="video/mp4">
      </video>
    </div>
    <div class="col-md-12" style="margin-top:30px;">
      <?= $form->field($model, 'video')->widget(FileInput::classname(), [
        'options' => ['accept' => 'video/*'],
        'pluginOptions' => [
          'showUpload' => false,
          'showPreview' => false,
          // 'browseLabel' => '',
          // 'removeLabel' => '',
          'mainClass' => 'input-group-lg'
        ]
        ])->label(false) ?>
        <div class="form-group">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
