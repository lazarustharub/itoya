<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=Yii::$app->urlManager->createUrl(['/images/itoya-logo.jpg'])?>" class="img-circle" alt="User Image" style="height:45px;"/>
            </div>
            <div class="pull-left info">
                <p><?php echo Yii::$app->user->identity->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Itoya Detail', 'options' => ['class' => 'header']],
                    ['label' => 'Promotion Video', 'icon' => 'play', 'url' => ['promotion-video/']],
                    ['label' => 'Delivery Services', 'icon' => 'users', 'url' => ['delivery/']],
                    ['label' => 'Info', 'icon' => 'bullhorn', 'url' => ['info/']],
                    ['label' => 'Caption', 'icon' => 'etsy', 'url' => ['caption/']],
                    ['label' => 'Master Data Itoya', 'options' => ['class' => 'header']],
                    ['label' => 'Master City', 'icon' => 'building', 'url' => ['distribution-city/']],
                    ['label' => 'Master Location', 'icon' => 'map-marker', 'url' => ['distribution-location/']],
                    ['label' => 'Master Food', 'icon' => 'cutlery', 'url' => ['distribution-category/']],

                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>



    </section>

</aside>
