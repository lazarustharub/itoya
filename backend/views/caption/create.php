<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Caption */

$this->title = 'Create Caption';
$this->params['breadcrumbs'][] = ['label' => 'Captions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caption-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
