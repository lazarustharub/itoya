<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Caption */

$this->title = 'Update Caption: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Captions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="caption-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
