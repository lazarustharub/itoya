<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromotionVideo */

$this->title = 'Create Caption';
$this->params['breadcrumbs'][] = ['label' => 'Promotion Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caption-create">


  <div class="box box-primary">
    <div class="box-body">
      <div class="col-md-12">
        <?= $this->render('_form', [
          'model' => $model,
          ]) ?>
      </div>
    </div>
  </div>

</div>
