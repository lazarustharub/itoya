<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Info */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="info-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'fileinfo')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'required' => true,
        ],
        'pluginOptions' => [
            'showUpload' => false,
            // 'showPreview' => false,
            // 'browseLabel' => '',
            // 'removeLabel' => '',
            'mainClass' => 'input-group-lg'
        ]
    ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
