<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromotionVideo */

$this->title = 'Create Information Picture';
$this->params['breadcrumbs'][] = ['label' => 'Create Info Picture', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">


  <div class="box box-primary">
    <div class="box-body">
      <div class="col-md-12">
        <?= $this->render('_form', [
          'model' => $model,
          ]) ?>
      </div>
    </div>
  </div>

</div>
