<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PromotionVideo */

$this->title = 'Update Information Picture';
$this->params['breadcrumbs'][] = ['label' => 'Update Information Picture', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="information-update">

  <div class="box box-primary">
    <div class="box-body">
      <?= $this->render('_form-update', [
        'model' => $model,
        ]) ?>

    </div>
  </div>

</div>
