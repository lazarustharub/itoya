<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Info */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="info-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-md-12">
      <?php echo FileInput::widget([
        'model' => $model,
        'attribute' => 'fileinfo',
        'pluginOptions' => [
          'initialPreview'=>[
            Html::img(Yii::$app->urlManager->createUrl(['/images/info/'.$model->picture]),['style' => 'width:200px;'])
          ],
          'overwriteInitial'=>true
        ]
      ]); ?>

    </div>

    <div class="col-md-12" style="margin-top:15px;">
      <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
      </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
