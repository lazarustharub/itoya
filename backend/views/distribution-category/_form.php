<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DistributionCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribution-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <label>Data Katergori Baru</label>
      </div>
      <div class="box-body">
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Nama Kategori</label>
          </div>
          <div class="col-md-5">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
          </div>
        </div>
        <div class="form-group col-md-12">
          <div class="col-md-2">
            <label>Kota</label>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'city_id')->dropDownList(
            ArrayHelper::map(\common\models\DistributionCity::find()->asArray()->all(), 'id', 'name'),['prompt'=>"Pilih Kota ..."]
            )->label(false) ?>
          </div>
        </div>

        <div class="form-group">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
      </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
