<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionCategory */

$this->title = 'Create Distribution Category';
$this->params['breadcrumbs'][] = ['label' => 'Distribution Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
