<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DistributionCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Distribution Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-category-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Distribution Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
              'label' => 'Nama Kategori',
              'attribute' => 'name',
            ],
            // 'city_id',
            [
              'label' => 'Kota',
              'attribute' => 'city_id',
              'value' => 'city.name',
              'filter'=>ArrayHelper::map(\common\models\DistributionCity::find()->asArray()->all(), 'id', 'name'),
            ],

            [
              'class' => 'yii\grid\ActionColumn',
              'header' => 'Actions',
              'headerOptions' => ['style' => 'color:#337ab7'],
              'template' => '{menu} {update} {delete}',
              'buttons' => [
                'menu' => function ($url, $model) {
                    return Html::a('<span class="btn btn-sm btn-warning"> MENU <i class="glyphicon glyphicon-cutlery"></i></span>', $url, [
                                'title' => Yii::t('app', 'Daftar Menu'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="btn btn-sm btn-success"><i class="glyphicon glyphicon-pencil"></i></span>', $url, [
                                'title' => Yii::t('app', 'Ubah'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></span>', $url, [
                                'title' => Yii::t('app', 'Hapus'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                                'data-method' => 'post', 'data-pjax' => '0',

                    ]);
                },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'menu') {
                    $url = Url::to(['distribution-food/index','id'=>$model['id']]);
                    return $url;
                }
                if ($action === 'update') {
                    $url = Url::to(['distribution-category/update','id'=>$model['id']]);
                    return $url;
                }
                if ($action === 'delete') {
                    $url = Url::to(['distribution-category/delete','id'=>$model['id']]);
                    return $url;
                }

              }
          ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
