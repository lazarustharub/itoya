<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionCategory */

$this->title = 'Update Distribution Category: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Distribution Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribution-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
