<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionCity */

$this->title = 'Create Distribution City';
$this->params['breadcrumbs'][] = ['label' => 'Distribution Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribution-city-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
