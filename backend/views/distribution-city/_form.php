<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribution-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <label>Data Kota Baru</label>
      </div>
      <div class="box-body">
        <div class="col-md-4">
          <label>Nama Kota</label>
        </div>
        <div class="col-md-4">
          <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
