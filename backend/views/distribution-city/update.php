<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DistributionCity */

$this->title = 'Update Distribution City: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Distribution Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribution-city-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
