<?php

namespace backend\controllers;

use Yii;
use common\models\DistributionFood;
use common\models\DistributionFoodSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DistributionFoodController implements the CRUD actions for DistributionFood model.
 */
class DistributionFoodController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DistributionFood models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new DistributionFoodSearch();
        $dataProvider = $searchModel->searchbycategory(Yii::$app->request->queryParams, $id);
        $category_id = $_GET['id'];

        // var_dump($id);die;


        return $this->render('index', [
            'category_id' => $category_id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DistributionFood model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DistributionFood model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {

        // var_dump($id);die;
        $model = new DistributionFood();

        if ($model->load(Yii::$app->request->post())) {

          $file = UploadedFile::getInstance($model,'gambar');
          $uploadPath = Yii::getAlias('@backend')."/web/images/food/";
          $fileName = "itoya_".$file->baseName.".".$file->extension;
          $fileDirectory = $uploadPath.$fileName;

          if($file->saveAs($fileDirectory)){
            $model->picture = $fileName;
            $model->category_id = $id;
            if($model->save(false)){
              Yii::$app->getSession()->setFlash('success', 'Data Saved !');
              return $this->redirect(['index', 'id' => $id]);
            }
          }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DistributionFood model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $category_id)
    {
        $model = $this->findModel($id);
        $oldPic = $model->picture;

        if ($model->load(Yii::$app->request->post())) {
          $file = UploadedFile::getInstance($model,'gambar');
          if(isset($file)){
            $uploadPath = Yii::getAlias('@backend')."/web/images/food/";
            $fileName = "itoya_".$file->baseName.".".$file->extension;
            $fileDirectory = $uploadPath.$fileName;

            if($file->saveAs($fileDirectory)){
              // var_dump($file);die;
              $model->picture = $fileName;
              if($model->save(false)){
                Yii::$app->getSession()->setFlash('success', 'Data Saved !');
                return $this->redirect(['index', 'id' => $category_id]);
              }
            }
          }else{
            $model->picture = $oldPic;
            if($model->save(false)){
              Yii::$app->getSession()->setFlash('success', 'Data Saved !');
              return $this->redirect(['index', 'id' => $category_id]);
            }
          }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DistributionFood model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $category_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'id' => $category_id]);
    }

    /**
     * Finds the DistributionFood model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DistributionFood the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DistributionFood::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
