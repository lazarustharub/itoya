<?php

namespace backend\controllers;

use Yii;
use common\models\PromotionVideo;
use common\models\PromotionVideoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PromotionVideoController implements the CRUD actions for PromotionVideo model.
 */
class PromotionVideoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PromotionVideo models.
     * @return mixed
     */
    public function actionIndex()
    {

      $model = PromotionVideo::find();

      if(!isset($model)){
        $model = new PromotionVideo();

        if ($model->load(Yii::$app->request->post())) {
          $video = UploadedFile::getInstance($model, 'video');
          $uploadPath = Yii::getAlias('@backend')."/web/videos/";
          $fileName ="itoya_".$video->baseName.".".$video->extension;
          $fileDirectory = $uploadPath . $fileName;
          if($video->saveAs($fileDirectory)){
            $model->link = $fileName;
            if($model->save(false)){
              // var_dump($model->link);die;
              return $this->redirect(Yii::$app->request->referrer);
            }
          }
        }

        return $this->render('index', [
          'model' => $model,
        ]);
      }else{

        $model = PromotionVideo::find()->one();
        $oldVideo = $model->link;

        if ($model->load(Yii::$app->request->post())) {
          $video = UploadedFile::getInstance($model, 'video');
          // var_dump($video);die;
          if(isset($video)){
            $uploadPath = Yii::getAlias('@backend')."/web/videos/";
            $fileName ="itoya_".$video->baseName.".".$video->extension;
            $fileDirectory = $uploadPath . $fileName;
            if($video->saveAs($fileDirectory)){
              $model->link = $fileName;
              if($model->save(false)){
                return $this->redirect(Yii::$app->request->referrer);
              }
            }
          }else{

            // var_dump("AUUU");die;
            $model->link = $oldVideo;
            if($model->save(false)){
              return $this->redirect(Yii::$app->request->referrer);
            }
          }
        }

        return $this->render('update', [
          'model' => $model,
          'oldVideo' => $oldVideo,
        ]);

      }

    }

    /**
     * Displays a single PromotionVideo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PromotionVideo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PromotionVideo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PromotionVideo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PromotionVideo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PromotionVideo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PromotionVideo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PromotionVideo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
