<?php

namespace backend\controllers;

use Yii;
use common\models\DistributionLocation;
use common\models\DistributionLocationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DistributionLocationController implements the CRUD actions for DistributionLocation model.
 */
class DistributionLocationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DistributionLocation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DistributionLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DistributionLocation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DistributionLocation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DistributionLocation();

        if ($model->load(Yii::$app->request->post())){

            $file = UploadedFile::getInstance($model,'gambar');
            $uploadPath = Yii::getAlias('@backend')."/web/images/location/";
            $fileName = "itoya_".$file->baseName.".".$file->extension;
            $fileDirectory = $uploadPath.$fileName;

            if($file->saveAs($fileDirectory)){
              $model->picture = $fileName;
              if($model->save(false)){
                Yii::$app->getSession()->setFlash('success', 'Data Saved !');
                return $this->redirect(['index']);
              }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DistributionLocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPic = $model->picture;

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model,'gambar');

            if(isset($file)){
              $uploadPath = Yii::getAlias('@backend')."/web/images/location/";
              $fileName = "itoya_".$file->baseName.".".$file->extension;
              $fileDirectory = $uploadPath.$fileName;

              if($file->saveAs($fileDirectory)){
                $model->picture = $fileName;
                if($model->save()){
                  Yii::$app->getSession()->setFlash('success', 'Data Saved !');
                  return $this->redirect(['index']);
                }
              }
            }else{
              $model->picture = $oldPic;
              if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'Data Saved !');
                return $this->redirect(['index']);
              }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DistributionLocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DistributionLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DistributionLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DistributionLocation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
