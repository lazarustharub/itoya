<?php

namespace backend\controllers;

use Yii;
use common\models\Info;
use common\models\InfoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InfoController implements the CRUD actions for Info model.
 */
class InfoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Info models.
     * @return mixed
     */
    public function actionIndex()
    {
      $model = Info::find();

      if(isset($model)){
        $model = Info::find()->one();
        $oldPic = $model->picture;

        if ($model->load(Yii::$app->request->post())) {
          $file = UploadedFile::getInstance($model,'fileinfo');


          // var_dump($model->fileinfo);die;

          if(isset($file)){
            $uploadPath = Yii::getAlias('@backend')."/web/images/info/";
            $fileName ="itoya_".$file->baseName.".".$file->extension;
            $fileDirectory = $uploadPath . $fileName;
            if($file->saveAs($fileDirectory)){
              $model->picture = $fileName;
              if($model->save(false)){
                Yii::$app->getSession()->setFlash('success', 'File Uploaded !');
                return $this->redirect(Yii::$app->request->referrer);
              }
            }
          }else{
            // var_dump("sad");die;

            $model->picture = $oldPic;
            if($model->save(false)){
              Yii::$app->getSession()->setFlash('info', 'No Change Made !');
              return $this->redirect(Yii::$app->request->referrer);
            }
          }
        }

        return $this->render('update', [
          'model' => $model,
        ]);
      }else{
        $model = new Info();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model,'fileinfo');
            $uploadPath = Yii::getAlias('@backend')."/web/images/info/";
            $fileName = "itoya_".$file->baseName.".".$file->extension;
            $fileDirectory = $uploadPath . $fileName;

            if($file->saveAs($fileDirectory)){
              $model->picture = $fileName;
              if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'File Uploaded !');
                return $this->redirect(['index']);
              }else{
                Yii::$app->getSession()->setFlash('danger', 'Upload Fail !');
                return $this->redirect(Yii::$app->request->referrer);

              }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
      }


    }

    /**
     * Displays a single Info model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Info model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Info();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model,'fileinfo');
            $uploadPath = Yii::getAlias('@backend')."/web/images/info/";
            $fileName = "itoya_".$file->baseName.".".$file->extension;
            $fileDirectory = $uploadPath . $fileName;

            if($file->saveAs($fileDirectory)){
              $model->picture = $fileName;
              $model->save();
              return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Info model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    /**
     * Deletes an existing Info model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Info model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Info the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Info::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
