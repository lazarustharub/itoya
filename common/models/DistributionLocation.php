<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "distribution_location".
 *
 * @property int $id
 * @property string $name
 * @property string $open_hour
 * @property string $close_hour
 * @property string $phone
 * @property string $picture
 * @property string $location
 * @property string $location_x
 * @property string $location_y
 * @property int $city_id
 *
 * @property DistributionCity $city
 */
class DistributionLocation extends \yii\db\ActiveRecord
{
    public $gambar;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribution_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city_id'], 'required'],
            [['open_hour', 'close_hour'], 'safe'],
            [['city_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['gambar'],'file', 'skipOnEmpty'=>true,'extensions' => 'jpg,png,jpeg'],
            [['phone', 'picture', 'location_x', 'location_y'], 'string'],
            [['location'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => DistributionCity::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'open_hour' => 'Open Hour',
            'close_hour' => 'Close Hour',
            'phone' => 'Phone',
            'picture' => 'Picture',
            'location' => 'Location',
            'location_x' => 'Location X',
            'location_y' => 'Location Y',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(DistributionCity::className(), ['id' => 'city_id']);
    }
}
