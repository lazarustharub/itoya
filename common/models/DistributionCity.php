<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "distribution_city".
 *
 * @property int $id
 * @property string $name
 *
 * @property DistributionCategory[] $distributionCategories
 * @property DistributionLocation[] $distributionLocations
 */
class DistributionCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribution_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributionCategories()
    {
        return $this->hasMany(DistributionCategory::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributionLocations()
    {
        return $this->hasMany(DistributionLocation::className(), ['city_id' => 'id']);
    }
}
