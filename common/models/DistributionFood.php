<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "distribution_food".
 *
 * @property int $id
 * @property string $name
 * @property string $picture
 * @property int $category_id
 *
 * @property DistributionCategory $category
 */
class DistributionFood extends \yii\db\ActiveRecord
{
    public $gambar;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribution_food';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gambar'],'file','skipOnEmpty'=>true,'extensions'=>'jpg,png,jpeg'],
            [['name', 'picture', 'category_id'], 'required'],
            [['category_id'], 'integer'],
            [['name', 'picture'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DistributionCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'picture' => 'Picture',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DistributionCategory::className(), ['id' => 'category_id']);
    }
}
