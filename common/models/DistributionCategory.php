<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "distribution_category".
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 *
 * @property DistributionCity $city
 * @property DistributionFood[] $distributionFoods
 */
class DistributionCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribution_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => DistributionCity::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(DistributionCity::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributionFoods()
    {
        return $this->hasMany(DistributionFood::className(), ['category_id' => 'id']);
    }
}
