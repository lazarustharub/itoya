<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "info".
 *
 * @property int $id
 * @property string $picture
 */
class Info extends \yii\db\ActiveRecord
{

    public $fileinfo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fileinfo'], 'file', 'skipOnEmpty'=>true,'extensions' => 'jpg, png,jpeg'],
            [['picture'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'picture' => 'Picture',
        ];
    }
}
