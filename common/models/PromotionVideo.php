<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promotion_video".
 *
 * @property int $id
 * @property string $link
 */
class PromotionVideo extends \yii\db\ActiveRecord
{

    public $video;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promotion_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'safe'],
            [['video'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp4'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
        ];
    }
}
