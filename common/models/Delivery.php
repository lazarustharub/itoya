<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property string $name
 * @property string $picture
 */
class Delivery extends \yii\db\ActiveRecord
{

    public $gambar;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','link'], 'string', 'max' => 45],
            [['gambar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg,png,jpeg'],
            [['picture'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'picture' => 'Picture',
        ];
    }
}
