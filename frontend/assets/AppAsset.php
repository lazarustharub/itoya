<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'assets/css/bootstrap.min.css',
        'assets/css/scrolling-nav.css',
        'assets/css/main.css',
        'assets/scss/custom.css',
        'assets/css/animate.css',
        'assets/css/owl.carousel.css',
        'assets/css/owl.theme.css',
        'assets/css/responsive.css',
        'https://use.fontawesome.com/releases/v5.7.2/css/all.css',
    ];
    public $js = [
        'assets/js/jquery.min.js',
        'assets/js/tether.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/owl.carousel.min.js',
        'assets/js/form-validator.min.js',
        'assets/js/contact-form-script.js',
        'assets/js/jquery.easing.min.js',
        'assets/js/scrolling-nav.js',
        'assets/js/main.js',
        'assets/js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
