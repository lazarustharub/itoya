<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Bootstrap UI Kit">
    <meta name="keywords" content="ui kit">
    <meta name="author" content="UIdeck">

    <title>ITOYA - Tokyo In A Bowl</title>

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<style type="text/css">
  @media (pointer: coarse) and (hover: none) {
    #videoStyle video {
      display: none;
    }
  }
</style>

<div class="wrapper">

  <section id="headertop-itoya">
    <header id="imageStyle" style="background-image: url('assets/img/bg/itoyabg.jpg');">  
      <div class="overlay">
        
      </div>
      <div class="container img-head h-100">
        <div class="d-flex my-auto h-100 text-center align-items-center">
          <img src="assets/img/itoya.png" class="mx-auto d-block align-middle" alt="">
        </div>
      </div>
      <div class="container scroll-bottom w-100 d-block text-center" style="position: absolute; bottom: 70px;">
        <a href="#header-itoya" style="padding: 30px;"><i style="color: #ffffff;"></i></a>
      </div>
    </header>
  </section>

  <!-- Content Area Start -->
  <nav class="navbar navbar-toggleable-sm navbar-light bg-default" id="navbarSpy">
    <div class="container">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar5" aria-expanded="false" aria-label="Toggle navigation">
         <i class="fa fa-bars"></i>
       </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbar5">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#about-itoya">ABOUT US</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#dish-itoya">MENU</a>
          </li>
          <li class="nav-item hide-menu">
            <a class="nav-link" href="#contact-itoya">LOCATION</a>
          </li>
          <li class="nav-item hide-menu">
            <a class="nav-link" href="#contact-itoya">CONTACT</a>
          </li>
        </ul>
      </div>
      <a class="navbar-brand" href="#headertop-itoya"><img src="assets/img/itoya.png" alt=""></a>
      <div class="collapse navbar-collapse justify-content-left">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#contact-itoya">LOCATION</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact-itoya">CONTACT</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>




  <section id="header-itoya">
    <header id="videoStyle">
      <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="<?php
        $video = \common\models\PromotionVideo::find()->one();
        echo Yii::$app->urlManagerBackend->createUrl(['/videos/'.$video->link])?>" type="video/mp4">
      </video>
    </header>
  </section>

  <section id="about-itoya">
    <div id="content-about">
      <div class="cust-container">
        <div class="row">
          <div class="col-md-12">
            <div class="page-header-title">
              <h1 class="heading-title text-center"><?php $title = \common\models\Caption::find()->one(); ?><?= $title->title; ?></h1>
            </div>
            <div class="sub-title">
              <p class="lead"><?= $title->description; ?></p>
            </div>

            <!-- navbar-default -->
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="dish-itoya">
    <div id="content-dish">
      <!-- light slider -->
      <div id="light-slider" class="carousel slide">
        <div id="carousel-area">
          <div id="carousel-slider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-slider" data-slide-to="1"></li>
              <li data-target="#carousel-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img src="assets/img/slider/food1.jpg" alt="">
              </div>
              <div class="carousel-item">
                <img src="assets/img/slider/food1.jpg" alt="">
              </div>
              <div class="carousel-item">
                <img src="assets/img/slider/food1.jpg" alt="">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
              <span class="carousel-control carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
              <span class="carousel-control carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      <div class="page-header-title text-center">
          <h1 class="heading-title">MENU</h1>
          <a class="show-more" data-toggle="collapse" href="#menuiItoya" role="button" aria-expanded="false" aria-controls="menuiItoya">
            Show More <i class="fa fa-chevron-down"></i>
          </a>
          <div class="collapse" id="menuiItoya">
            <div class="card card-body">
              <ul class="nav nav-pills mb-3 justify-content-center" id="menu-tab" role="tablist">
                <?php $getCities = \common\models\DistributionCity::find()->all();
                foreach ($getCities as $key => $city) {
                  $cityName = str_replace(' ','',$city->name);
                  $cityNameLow = strtolower($cityName); ?>
                      <li class="nav-item">
                        <a class="nav-link <?php if($key==0){echo "active";} ?>" id="menu-<?=$cityNameLow?>-tab" data-toggle="pill" href="#menu-<?=$cityNameLow?>" role="tab" aria-controls="menu-<?=$cityNameLow?>" aria-selected="true"><?php echo $city->name; ?></a>
                      </li>
                    <?php }
                 ?>
              </ul>
              <div class="tab-content itoya-img-menu" id="menu-tabContent">
                <?php $getCities = \common\models\DistributionCity::find()->all();
                  foreach ($getCities as $key => $city) {
                    $cityName = str_replace(' ','',$city->name);
                    $cityNameLow = strtolower($cityName);?>

                    <div class="tab-pane fade <?php if($key == 0) {echo "show active";} ?>" id="menu-<?=$cityNameLow?>" role="tabpanel" aria-labelledby="menu-<?=$cityNameLow?>-tab">

                      <?php foreach ($city->distributionCategories as $key => $category) { ?>
                        <div class="cust-container" style="margin-top: 20px;">
                          <h4><?= $category->name; ?></h4>
                          <div class="row">
                            <?php foreach ($category->distributionFoods as $key => $food) { ?>

                              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 col-half-offset text-center">
                                <img src="<?=Yii::$app->urlManagerBackend->createUrl(['/images/food/'.$food->picture])?>" >
                                <p><?= $food->name ?></p>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                      <?php } ?>

                    </div>

                  <?php }
                 ?>

                <!-- <div class="tab-pane fade" id="menu-malang" role="tabpanel" aria-labelledby="menu-malang-tab">Malang</div> -->
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>

  <section id="contact-itoya">
    <div id="content-contact">
      <div class="row">
        <div class="col-md-12">
          <div class="page-header-title">
            <h1 class="heading-title text-center">LOCATIONS</h1>
          </div>
          <ul class="nav nav-pills mb-3 justify-content-center" id="loc-tab" role="tablist">
            <?php $getCities = \common\models\DistributionCity::find()->all();
            foreach ($getCities as $key => $city) {
              $cityName = str_replace(' ','',$city->name);
              $cityNameLow = strtolower($cityName); ?>
                <li class="nav-item">
                  <a class="nav-link <?php if($key==0){echo "active";} ?>" id="loc<?= $cityNameLow?>tab" data-toggle="pill" href="#loc<?=$cityName?>" role="tab" aria-controls="loc<?=$cityName?>" aria-selected="true"><?=$city->name ?></a>
                </li>
                <?php }
             ?>

          </ul>


          <div class="tab-content" id="loc-tabContent">

            <?php $getCities = \common\models\DistributionCity::find()->all();

            foreach ($getCities as $key => $city) {
              $cityName = str_replace(' ','',$city->name);
              $cityNameLow = strtolower($cityName);

              ?>

                <div class="tab-pane fade <?php if($key==0){echo "show active";} ?>" id="loc<?=$cityName?>" role="tabpanel" aria-labelledby="loc<?=$cityNameLow?>tab">
                <!-- Itoya Surabaya -->
                <ul class="nav nav-tabs justify-content-center" id="detailLocSub-tab" role="tablist">
                  <?php
                  foreach ($city->distributionLocations as $key2 => $location) { ?>
                    <li class="nav-item">
                      <a class="nav-link <?php if($key2==0){echo "active";} ?>" id="detailLocSub<?=$cityNameLow.$location->id?>" data-toggle="tab" href="#detailLocSub<?=$location->id?>" role="tab" aria-controls="detailLocSubtp" aria-selected="true"><?= $location->name;  ?></a>
                    </li>
                  <?php }?>
                </ul>
                <div class="tab-content" id="detailLocSub-tabContent">

                  <?php

                  foreach ($city->distributionLocations as $key3 => $location) { ?>
                      <div class="tab-pane fade <?php if($key3==0){echo "show active";} ?>" id="detailLocSub<?=$location->id?>" role="tabpanel" aria-labelledby="detailLocSub<?=$cityNameLow.$location->id?>">
                      <!-- Itoya TP -->
                        <div class="row itoya-location">
                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="row clearfix">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?q=<?= $location->location_x ?>,<?= $location->location_y ?>&amp;key=AIzaSyDEJifTz-2J9QyeCN9F45uNcSozkeLqSaI">
                                </iframe>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <img src="<?=Yii::$app->urlManagerBackend->createUrl(['/images/location/'.$location->picture])?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 align-self-center">
                            <div class="cust-container">
                              <address>
                                <strong>Address:</strong> <?=$location->location ?><br>
                                <!-- Surabaya City, East Java 60261<br> -->
                                <strong>Open-Close:</strong> <?= $location->open_hour."-".$location->close_hour ?><br>
                                <abbr title="Phone"><strong>Phone:</strong></abbr> <?= $location->phone ?>
                              </address>
                            </div>
                          </div>
                        </div>
                        <!-- End Itoya TP -->
                        </div>
                  <?php } ?>

              </div>
              <!-- End Itoya Surabaya -->

            </div>

            <?php } ?>



          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="promo-itoya">
    <div id="content-promo">
      <div class="row h-100 justify-content-center align-items-center">
        <img src="<?php
        $info = \common\models\Info::find()->one();
        echo Yii::$app->urlManagerBackend->createUrl(['/images/info/'.$info->picture])?>" class="mx-auto d-block align-middle" alt="">
      </div>
    </div>
  </section>

  <section id="deliver-itoya">
    <div id="content-deliver">
      <div class="cust-container">
        <div class="page-header-title">
          <h1 class="heading-title text-center">WE DELIVER!</h1>
        </div>
        <div class="row text-center justify-content-center">
          <?php $getDeliveries = \common\models\Delivery::find()->all();
          foreach ($getDeliveries as $key => $delivery) { ?>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 col-half-offset">
              <img src="<?= Yii::$app->urlManagerBackend->createUrl(['/images/delivery/'.$delivery->picture])?>" alt="" class="img-fluid mx-auto d-block">
            </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </section>

  <!-- Content Area End -->
</div>
<!-- Page Wrapper End -->
<section id="footer-itoya">
<!-- Light Footer -->
  <footer class="light-footer">
    <div class="cust-container">
      <div class="row">
        <div class="col-md-4 footer-content footer-content-1">
          <p><strong>Email:</strong> itoya.donburi@rfc.com </p>
        </div>
        <div class="col-md-4 footer-content footer-content-2">
          <p><strong>Phone:</strong> (031) 5610912</p>
        </div>
        <div class="col-md-4 footer-content footer-content-3">
          <p>Designed by <a href="http://mamorasoft.com/" style="color: #000;">MAMORASOFT</a></p>
        </div>
      </div>
    </div>
  </footer>
</section>

<!-- jQuery first, then Tether, then Bootstrap JS. -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
